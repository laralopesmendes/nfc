﻿using NFC.Interfaces;
using NFC_PCSC;
using NUnit.Framework;
using System;

namespace NFC_Test.REAL
{
    [TestFixture, Explicit]
    public class REAL_Reader_PCSC
    {
        /// <summary>
        /// Print PCSC ReaderIDs to Console Out
        /// </summary>
        [Test]
        public void GetReaders()
        {
            IHardware hardware = new Hardware_PCSC();
            string[] readers = hardware.GetReaders();

            Console.WriteLine("PCSC Readers detected: {0}", readers.Length);

            if (readers.Length > 0)
            {
                Console.WriteLine("List of ReaderIDs:");
                foreach (string readerID in readers)
                {
                    Console.WriteLine("{0}", readerID);
                }
            }
        }

        /// <summary>
        /// Connect to specific PCSC Reader by ReaderID
        /// </summary>
        /// <param name="readerID">ReaderID from GetReaders</param>
        [TestCase("ACS ACR122U PICC Interface 0")]
        public void Connect(string readerID)
        {
            IHardware hardware = new Hardware_PCSC();
            IReader reader = hardware.OpenReader(readerID);

            bool connected_successfully = false;

            ReaderEventHandler handler = (sender, card) =>
            {
                card.Connect();

                connected_successfully = true;

                card.Disconnect();
            };

            reader.CardDiscovered += handler;
            reader.Start();

            Assert.AreEqual(true, connected_successfully);

            reader.Stop();
            reader.CardDiscovered -= handler;
        }
    }
}
