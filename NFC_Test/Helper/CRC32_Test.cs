﻿using NFC.Helper;
using NFC.Helper.Crypto.CRC;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    [TestFixture]
    public class CRC32_Test
    {
        [Test]
        public void Caluclate()
        {
            byte[] data = HexConverter.ConvertFromHexString("c40045eeb8338ae8f49a032e85bb1114353010");
            byte[] crc_expected = HexConverter.ConvertFromHexString("95c3894b");

            CRC32 crc32 = new CRC32();
            byte[] crc = crc32.Calculate(data);

            Assert.AreEqual(crc_expected, crc);
        }
    }
}
