﻿using NFC.Helper;
using NFC.Helper.Crypto.Cipher;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    [TestFixture]
    public class AES_Test
    {
        [Test]
        public void Encrypt()
        {
            byte[] data = HexConverter.ConvertFromHexString("8db1f942f2d7cc82f6fa1486a30f8c12104a3b07e8eb77a7ac00000000000000");
            byte[] key = HexConverter.ConvertFromHexString("e7aff3361c3e85347993c3219a87d24b");
            byte[] iv = HexConverter.ConvertFromHexString("00000000000000000000000000000000");

            AES aes = new AES();
            byte[] data_enc = aes.Encrypt(data, key, iv);

            byte[] data_enc_expected = HexConverter.ConvertFromHexString("3c79d74a4969ba7123e5d8f6df24493112d221fd131a4617d0eda5d92ccc1b46");
            Assert.AreEqual(data_enc_expected, data_enc);
        }
    }
}