﻿using NFC.Helper;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    public class HexConverter_Test
    {
        [Test]
        public void ConvertFromHexString()
        {
            string s = "0180ff0a";

            byte[] expected_s =
            {
                0x01, 0x80, 0xFF, 0x0A
            };

            Assert.AreEqual(expected_s, HexConverter.ConvertFromHexString(s));
        }

        [Test]
        public void ConvertToHexString()
        {
            byte[] s =
            {
                0x01, 0x80, 0xFF, 0x0A
            };

            string expected_s = "0180ff0a";

            Assert.AreEqual(expected_s, HexConverter.ConvertToHexString(s));
        }
    }
}
