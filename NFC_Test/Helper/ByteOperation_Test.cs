﻿using NFC.Helper;
using NUnit.Framework;
using System;

namespace NFC_Test.Helper
{
    public class ByteOperation_Test
    {
        [Test]
        public void GenerateEmptyArray()
        {
            uint i = 16;

            byte[] data = ByteOperation.GenerateEmptyArray(i);

            for (int e = 0; e < i; e++)
            {
                if (data[e] != 0x00)
                {
                    Assert.Fail("Data is not 0x00");
                }
            }
        }

        [Test]
        public void GetSubArray()
        {
            byte[] array = new byte[]
            {
                0x01, 0x02, 0x03, 0x04, 0x05
            };

            byte[] expected_subarray = new byte[]
            {
                0x02, 0x03, 0x04
            };

            Assert.AreEqual(expected_subarray, ByteOperation.GetSubArray(array, 1, 3));
        }

        [Test]
        public void ExtractLastBlock()
        {
            byte[] data = new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
            };

            byte[] expected_lastblock = new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
            };

            byte[] lastblock = ByteOperation.ExtractLastBlock(data, 8);

            Assert.AreEqual(expected_lastblock, lastblock);
        }

        [Test]
        public void ExtractLastBlock_WrongBlocksize()
        {
            byte[] data = new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
            };

            Assert.Throws<ArgumentException>(
            delegate
            {
                byte[] lastblock = ByteOperation.ExtractLastBlock(data, 7);
            });
        }

        [Test]
        public void ExtractLastBlock_Null()
        {
            byte[] data = null;

            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.ExtractLastBlock(data, 7);
            });
        }

        [Test]
        public void ExpandToBlockSize()
        {
            byte[] data = new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x01
            };

            byte[] expected_lastblock = new byte[]
            {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00
            };

            byte[] lastblock = ByteOperation.ExpandToBlockSize(data, 8);

            Assert.AreEqual(expected_lastblock, lastblock);
        }

        [Test]
        public void ExpandToBlockSize_Null()
        {
            byte[] data = null;

            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.ExpandToBlockSize(data, 8);
            });
        }

        [Test]
        public void RotateLeft()
        {
            byte[] data = new byte[]
            {
                0x01, 0x02, 0x03, 0x04
            };

            byte[] expected_data_left = new byte[]
            {
                0x02, 0x03, 0x04, 0x01
            };

            byte[] data_left = ByteOperation.RotateLeft(data);

            Assert.AreEqual(expected_data_left, data_left);
        }

        [Test]
        public void RotateLeft_Null()
        {
            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.RotateLeft(null);
            });
        }

        [Test]
        public void RotateRight()
        {
            byte[] data = new byte[]
            {
                0x01, 0x02, 0x03, 0x04
            };

            byte[] expected_data_left = new byte[]
            {
                0x04, 0x01, 0x02, 0x03
            };

            byte[] data_left = ByteOperation.RotateRight(data);

            Assert.AreEqual(expected_data_left, data_left);
        }

        [Test]
        public void RotateRight_Null()
        {
            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.RotateRight(null);
            });
        }

        [Test]
        public void Concatenate()
        {
            byte[] data_a = new byte[]
            {
                0x01, 0x02, 0x03, 0x04
            };

            byte[] data_b = new byte[]
            {
                0x05, 0x06, 0x07, 0x08
            };

            byte[] expected_data_c = new byte[]
            {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08
            };

            byte[] data_c = ByteOperation.Concatenate(data_a, data_b);

            Assert.AreEqual(expected_data_c, data_c);
        }

        [Test]
        public void Concatenate_ABC()
        {
            byte[] data_a = new byte[]
            {
                0x01, 0x02, 0x03, 0x04
            };

            byte[] data_b = new byte[]
            {
                0x05, 0x06, 0x07, 0x08
            };

            byte[] data_c = new byte[]
            {
                0x09, 0xA0, 0xB0, 0xC0
            };

            byte[] expected_data_d = new byte[]
            {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xA0, 0xB0, 0xC0
            };

            byte[] data_d = ByteOperation.Concatenate(data_a, data_b, data_c);

            Assert.AreEqual(expected_data_d, data_d);
        }

        [Test]
        public void Concatenate_Null()
        {
            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.Concatenate(null, null);
            });
        }

        [Test]
        public void XOR()
        {
            byte[] data_a = new byte[]
            {
                0x00, 0xF0, 0x00, 0xF0
            };

            byte[] data_b = new byte[]
            {
                0x0F, 0x00, 0x0F, 0x00
            };

            byte[] expected_data_c = new byte[]
            {
                0x0F, 0xF0, 0x0F, 0xF0
            };

            byte[] data_c = ByteOperation.XOR(data_a, data_b);

            Assert.AreEqual(expected_data_c, data_c);
        }

        [Test]
        public void XOR_null()
        {
            Assert.Throws<ArgumentNullException>(
            delegate
            {
                byte[] lastblock = ByteOperation.XOR(null, null);
            });
        }
    }
}
