﻿using NFC.Interfaces;
using PCSC;

namespace NFC_PCSC
{
    public class Hardware_PCSC : IHardware
    {
        public string[] GetReaders()
        {
            var contextFactory = ContextFactory.Instance;
            using var context = contextFactory.Establish(SCardScope.System);
            return context.GetReaders();
        }

        public bool IsAvailable()
        {
            if(GetReaders().Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public IReader OpenReader(string readerID)
        {
            return new Reader_PCSC(readerID);
        }
    }
}
