﻿using NFC.Interfaces;
using PCSC;
using PCSC.Iso7816;
using System;

namespace NFC_PCSC
{

    public class Reader_PCSC : IReader, IDisposable
    {
        private string _ReaderID;
        private IContextFactory _ContextFactory;
        private ISCardContext _SCardContext;
        private IsoReader _ISOReader;
        private ICard _Card;

        public Reader_PCSC(string readerID)
        {
            _ReaderID = readerID;
        }

        public event ReaderEventHandler CardDiscovered;
        public event ReaderEventHandler CardLost;

        public void Dispose()
        {
            Stop();
        }

        public void Start()
        {
            _ContextFactory = ContextFactory.Instance;
            _SCardContext = _ContextFactory.Establish(SCardScope.System);
            
            _ISOReader = new IsoReader(_SCardContext);

            _Card = new Card_PCSC(_ISOReader, _ReaderID);

            CardDiscovered?.Invoke(this, _Card);
        }

        public void Stop()
        {
            CardLost?.Invoke(this, _Card);

            _ISOReader.Dispose();
            _SCardContext.Dispose();
        }
    }
}
