﻿using PCSC;
using PCSC.Iso7816;
using NFC.Interfaces;
using NFC;
using System;

namespace NFC_PCSC
{
    public class Card_PCSC : ICard
    {
        private readonly IsoReader _ISOReader;
        private readonly string _ReaderID;

        public Card_PCSC(IsoReader isoreader, string readerID)
        {
            _ISOReader = isoreader;
            _ReaderID = readerID;
        }
        public void Connect()
        {
            _ISOReader.Connect(_ReaderID, SCardShareMode.Shared, PCSC.SCardProtocol.Any);
        }

        public void Disconnect()
        {
            _ISOReader.Disconnect(SCardReaderDisposition.Eject);
        }

        public APDUResponse Transmit(APDUCommand apdu_cmd)
        {
            Response response = _ISOReader.Transmit(ConvertAPDUCommand(apdu_cmd));
            return Convert(response);
        }

        public CommandApdu ConvertAPDUCommand(APDUCommand apdu_cmd)
        {
            switch(apdu_cmd.Case)
            {
                case NFC.IsoCase.Case1:
                    return new CommandApdu(ConvertISOCase(apdu_cmd.Case), ConvertSCardProtocol(apdu_cmd.Protocol))
                    {
                        CLA = apdu_cmd.CLA,
                        INS = apdu_cmd.INS,
                        P1 = apdu_cmd.P1,
                        P2 = apdu_cmd.P2
                    };
                case NFC.IsoCase.Case2Short:
                    return new CommandApdu(ConvertISOCase(apdu_cmd.Case), ConvertSCardProtocol(apdu_cmd.Protocol))
                    {
                        CLA = apdu_cmd.CLA,
                        INS = apdu_cmd.INS,
                        P1 = apdu_cmd.P1,
                        P2 = apdu_cmd.P2,
                        Le = apdu_cmd.LE
                    };
                case NFC.IsoCase.Case3Short:
                    return new CommandApdu(ConvertISOCase(apdu_cmd.Case), ConvertSCardProtocol(apdu_cmd.Protocol))
                    {
                        CLA = apdu_cmd.CLA,
                        INS = apdu_cmd.INS,
                        P1 = apdu_cmd.P1,
                        P2 = apdu_cmd.P2,
                        Data = apdu_cmd.Data
                    };
                case NFC.IsoCase.Case4Short:
                    return new CommandApdu(ConvertISOCase(apdu_cmd.Case), ConvertSCardProtocol(apdu_cmd.Protocol))
                    {
                        CLA = apdu_cmd.CLA,
                        INS = apdu_cmd.INS,
                        P1 = apdu_cmd.P1,
                        P2 = apdu_cmd.P2,
                        Data = apdu_cmd.Data,
                        Le = apdu_cmd.LE
                    };
                default:
                    throw new Exception("Unknown IsoCase");
            }
        }

        public PCSC.Iso7816.IsoCase ConvertISOCase(NFC.IsoCase isoCase)
        {
            switch(isoCase)
            {
                case NFC.IsoCase.Case1:
                    return PCSC.Iso7816.IsoCase.Case1;
                case NFC.IsoCase.Case2Short:
                    return PCSC.Iso7816.IsoCase.Case2Short;
                case NFC.IsoCase.Case3Short:
                    return PCSC.Iso7816.IsoCase.Case3Short;
                case NFC.IsoCase.Case4Short:
                    return PCSC.Iso7816.IsoCase.Case4Short;
                default:
                    throw new Exception("Unknown IsoCase");
            }
        }

        public PCSC.SCardProtocol ConvertSCardProtocol(NFC.SCardProtocol sCardProtocol)
        {
            switch (sCardProtocol)
            {
                case NFC.SCardProtocol.UNSET:
                    return PCSC.SCardProtocol.Unset;
                case NFC.SCardProtocol.T0:
                    return PCSC.SCardProtocol.T0;
                case NFC.SCardProtocol.T1:
                    return PCSC.SCardProtocol.T1;
                case NFC.SCardProtocol.RAW:
                    return PCSC.SCardProtocol.Raw;
                case NFC.SCardProtocol.T15:
                    return PCSC.SCardProtocol.T15;
                case NFC.SCardProtocol.ANY:
                    return PCSC.SCardProtocol.Any;
                default:
                    throw new NotSupportedException("Unknown SCardProtocol");
            }
        }

        public APDUResponse Convert(Response response)
        {
            ResponseApdu responseApdu = response.Get(0);

            APDUResponse apduResponse = new APDUResponse()
            {
                SW1 = responseApdu.SW1,
                SW2 = responseApdu.SW2,
                Body = responseApdu.GetData()
            };

            return apduResponse;
        }
    }
}
