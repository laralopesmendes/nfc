﻿//using System;
//using System.Threading;
//using NFC.Interfaces;
//using CoreNFC;
//using Foundation;

//namespace NFC_iOS
//{
//    public class Card_iOS : ICard
//    {
//        private NFCTagReaderSession _session;
//        private INFCMiFareTag _tag;

//        public Card_iOS(NFCTagReaderSession session, INFCMiFareTag tag)
//        {
//            _session = session;
//            _tag = tag;
//        }

//        public void Connect()
//        {
//            var counter = new CountdownEvent(1);
//            NSError err = null;

//            _session.ConnectTo(_tag, (error) =>
//            {
//                err = error;
//                counter.Signal();
//            });

//            counter.Wait();

//            if (err != null)
//            {
//                throw new Exception(err.LocalizedDescription);
//            }
//        }

//        public void Disconnect()
//        {
//            // TODO: decide on which should be used
//            //_session.RestartPolling();
//            _session.InvalidateSession("card disconnect");
//        }

//        public APDUResponse Transmit(APDUCommand cmd)
//        {
//            var counter = new CountdownEvent(1);
//            byte[] buf = null;

//            _tag.SendMiFareIso7816Command(new NFCIso7816Apdu(NSData.FromArray(cmd.Data)), (response, sw1, sw2, NSError) =>
//            {
//                // reassembly the original apdu message
//                buf = new byte[response.Length + 2];
//                response.ToArray().CopyTo(buf, 0);
//                buf[response.Length + 0] = sw1;
//                buf[response.Length + 1] = sw2;
//                counter.Signal();
//            });

//            counter.Wait();

//            return new APDUResponse(buf);
//        }
//    }
//}
