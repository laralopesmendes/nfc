﻿namespace NFC
{
    public class APDUResponse
    {
        public byte SW1 { get; set; }
        public byte SW2 { get; set; }
        public byte[] Body { get; set; }
    }
}
