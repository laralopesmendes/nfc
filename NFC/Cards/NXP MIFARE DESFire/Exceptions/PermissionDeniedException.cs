﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Current configuration / status does not allow the requested command.
    /// 0x919D
    /// </summary>
    public class PermissionDeniedException : Exception
    {
        public PermissionDeniedException()
        {

        }

        public PermissionDeniedException(string message) : base(message)
        {

        }

        public PermissionDeniedException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
