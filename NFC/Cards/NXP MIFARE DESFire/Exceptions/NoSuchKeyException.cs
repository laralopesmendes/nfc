﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Invalid key number specified.
    /// 0x9140
    /// </summary>
    public class NoSuchKeyException : Exception
    {
        public NoSuchKeyException()
        {

        }

        public NoSuchKeyException(string message) : base(message)
        {

        }

        public NoSuchKeyException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
