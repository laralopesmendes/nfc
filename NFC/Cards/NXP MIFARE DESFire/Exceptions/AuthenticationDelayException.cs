﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Currently not allowed to authenticate. Keeptrying until full delay is spent.
    /// 0x91AD
    /// </summary>
    public class AuthenticationDelayException : Exception
    {
        public AuthenticationDelayException()
        {

        }

        public AuthenticationDelayException(string message) : base(message)
        {

        }

        public AuthenticationDelayException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
