﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Specified file number does not exist.
    /// 0x91F0
    /// </summary>
    public class FileNotFoundException : Exception
    {
        public FileNotFoundException()
        {

        }

        public FileNotFoundException(string message) : base(message)
        {

        }

        public FileNotFoundException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
