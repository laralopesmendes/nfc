﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// hold the Access Rights for changing application keys (Change Key command)
    /// </summary>
    public enum ChangeApplicationKey : byte
    {
        /// <summary>
        /// Application master key authentication is necessary to change any key (default)
        /// </summary>
        MASTERKEY = 0x00,
        /// <summary>
        /// Authentication with the key to be changed (same Key#) is necessary to change a key
        /// </summary>
        SAMEKEY = 0x0E,
        /// <summary>
        /// All keys (except application master key, see Bit 0) within this application are frozen
        /// </summary>
        ALLKEYS = 0x0F
    }
}
