﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// codes whether a change of the application master key settings is allowed
    /// </summary>
    public enum ChangeMasterKeySettings : byte
    {

        /// <summary>
        /// configuration not changeable anymore (frozen)
        /// </summary>
        FROZEN = 0x00,

        /// <summary>
        /// this configuration is changeable if authenticated with the application master key (default)
        /// </summary>
        WITHMASTERKEY = 0x08
    }
}
