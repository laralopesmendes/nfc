﻿using System;

namespace NFC.Interfaces
{
    /// <summary>
    /// Abstract representation of the platform specific NFC Hardware
    /// </summary>
    public interface IHardware
    {
        /// <summary>
        /// Check if the device has nfc support
        /// </summary>
        /// <returns>Returns true if the device supports NFC</returns>
        bool IsAvailable();

        /// <returns>Returns all available readers</returns>
        string[] GetReaders();

        /// <summary>
        /// Create a new reader instance from the specified id
        /// </summary>
        /// <returns>Returns the spatform specific reader that corresponds to the id</returns>
        /// <exception cref="ArgumentException">Invalid reader id</exception>
        IReader OpenReader(string readerID);
    }
}
