﻿namespace NFC.Interfaces
{
    public interface ICard
    {
        /// <summary>
        /// Connect to Smartcard
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnect from Smartcard
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Transmit APDU Command to Smartcard
        /// </summary>
        /// <param name="apdu_cmd">Application Protocol Data Unit Command - ISO 7816</param>
        /// <returns>Application Protocol Data Unit Response - ISO 7816</returns>
        APDUResponse Transmit(APDUCommand apdu_cmd);
    }
}
