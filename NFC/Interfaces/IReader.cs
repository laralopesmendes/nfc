﻿namespace NFC.Interfaces
{
    public delegate void ReaderEventHandler(object sender, ICard card);

    /// <summary>
    /// Abstraction of a platform-specifc reader that can communicate with NFC cards
    /// </summary>
    public interface IReader
    {
        /// <summary>
        /// Event that will be called when a new tag was discovered
        /// </summary>
        event ReaderEventHandler CardDiscovered;

        /// <summary>
        /// Event that will be called when a tag that is in use gets disconnected
        /// </summary>
        event ReaderEventHandler CardLost;

        void Start();

        void Stop();
    }
}
