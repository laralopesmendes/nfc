﻿namespace NFC.Helper.Crypto
{
    public enum CipherType
    {
        /// <summary>
        /// DES / Triple DES
        /// </summary>
        TDES,

        /// <summary>
        /// Triple DES with 2 DES Keys
        /// </summary>
        TDES_2K,

        /// <summary>
        /// Triple DES with 3 DES Keys
        /// </summary>
        TDES_3K,

        /// <summary>
        /// AES
        /// </summary>
        AES
    }
}
