﻿using System;
using System.Collections.Generic;

namespace NFC.Helper
{
    public static class ByteOperation
    {
        /// <summary>
        /// Generate Byte Array filled with 0
        /// </summary>
        /// <param name="size">Size of Array</param>
        public static byte[] GenerateEmptyArray(uint size)
        {
            byte[] key = new byte[size];
            for (int i = 0; i < size; i++)
            {
                key[i] = 0;
            }

            return key;
        }

        /// <summary>
        /// Get Range of Array Elements
        /// </summary>
        /// <param name="array">Array</param>
        /// <param name="offset">Offset in Byte</param>
        /// <param name="length">Lenght to read in Byte</param>
        /// <returns>new Array with Range of Array Elements</returns>
        public static byte[] GetSubArray(byte[] array, long offset, long length)
        {
            byte[] subarray = new byte[length];
            for (long i = offset; i < offset + length; i++)
            {
                subarray[i - offset] = array[i];
            }
            return subarray;
        }

        /// <summary>
        /// Return a copy of the last Block of data
        /// </summary>
        /// <param name="data">Data compatible to blocksize</param>
        /// <param name="blocksize">in byte</param>
        public static byte[] ExtractLastBlock(byte[] data, uint blocksize)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null.");
            }

            if (data.Length % blocksize != 0)
            {
                throw new ArgumentException(string.Format("Data is not compatible with blocksize(data(length):{0}, blocksize:{1}.", data.Length, blocksize));
            }

            byte[] lastblock = new byte[blocksize];

            for (int i = 0; i < blocksize; i++)
            {
                lastblock[i] = data[data.Length - blocksize + i];
            }

            return lastblock;
        }

        /// <summary>
        /// Expand Array to Block Size, fill with 0x00
        /// </summary>
        /// <param name="data"></param>
        public static byte[] ExpandToBlockSize(byte[] data, uint bocksize)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null.");
            }

            int diff = data.Length % (int)bocksize;
            if (diff == 0)
            {
                return data;
            }

            byte[] expand = new byte[data.Length + bocksize - diff];

            data.CopyTo(expand, 0);

            for (int i = expand.Length - 1; i > data.Length - 1; i--)
            {
                expand[i] = 0x00;
            }

            return expand;
        }

        /// <summary>
        /// Rotates Array to the left
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Copy of data</returns>
        public static byte[] RotateLeft(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null.");
            }

            byte[] rotate = new byte[data.Length];
            data.CopyTo(rotate, 0);

            byte tmp = rotate[0];
            for (var i = 0; i < rotate.Length - 1; i++)
            {
                rotate[i] = rotate[i + 1];
            }
            rotate[rotate.Length - 1] = tmp;

            return rotate;
        }

        /// <summary>
        /// Rotates Array to the right
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Copy of data</returns>
        public static byte[] RotateRight(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null.");
            }

            byte[] rotate = new byte[data.Length];
            data.CopyTo(rotate, 0);

            byte tmp = rotate[rotate.Length - 1];
            for (var i = rotate.Length - 1; i > 0; i--)
            {
                rotate[i] = rotate[i - 1];
            }
            rotate[0] = tmp;

            return rotate;
        }

        /// <summary>
        /// Concatenates Arrays
        /// </summary>
        /// <param name="data">List of Byte Array</param>
        public static byte[] Concatenate(params byte[][] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null.");
            }

            List<byte> cat = new List<byte>();

            foreach (byte[] d in data)
            {
                cat.AddRange(d);
            }

            return cat.ToArray();
        }

        /// <summary>
        /// Boolean Operation XOR on all Bytes
        /// </summary>
        /// <param name="a">Array A</param>
        /// <param name="b">Array B</param>
        /// <returns>Copy of Data</returns>
        public static byte[] XOR(byte[] a, byte[] b)
        {
            if (a == null)
            {
                throw new ArgumentNullException("Array A cannot be null.");
            }

            if (b == null)
            {
                throw new ArgumentNullException("Array B cannot be null.");
            }

            if (a.Length != b.Length)
            {
                throw new ArgumentException(string.Format("Arrays are not same Length(Length A:{0}, Lenght B:{1})", a.Length, b.Length));
            }

            byte[] c = new byte[a.Length];

            for (int i = 0; i < a.Length; i++)
            {
                c[i] = (byte)(a[i] ^ b[i]);
            }

            return c;
        }
    }
}
